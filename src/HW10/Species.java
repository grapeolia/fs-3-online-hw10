package HW10;

public enum Species {
    DOMESTIC_CAT,
    DOG,
    ROBOCAT,
    PARROT,
    RABBIT,
    BIRD,
    FISH,
    UNKNOWN
}
