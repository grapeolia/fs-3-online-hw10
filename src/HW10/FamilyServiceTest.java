package HW10;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.*;

import static HW10.HappyFamily.setUpDailySchedule;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class FamilyServiceTest {
    private FamilyService module;

    @BeforeEach
    public void setUp(){

        Set<String> petHabits = Set.of("eat","sleep","jump");
        Pet simpleDomesticCat = new DomesticCat();
        Pet fullDog = new Dog("Tofik",2,34,petHabits);


        Man petrenkoFather = new Man("Father", "Petrenko", 1974);
        Woman petrenkoMother = new Woman("Mother", "Petrenko", 1979,78,setUpDailySchedule());
        Family petrenkoFamilyWithoutChildren = new Family(petrenkoMother,petrenkoFather);

        Man sydorenkoFather = new Man("Father", "Sydorenko", 1974);
        Woman sydorenkoMother = new Woman("Mother", "Sydorenko", 1979,78,setUpDailySchedule());
        Human sydorenkoBoyChild = new Human("childBoy","Sydorenko",2004);
        List<Human> sydorenkoChildren = List.of(sydorenkoBoyChild);
        Set<Pet> sydorenkoPets = Set.of(simpleDomesticCat);
        Family sydorenkoFamily = new Family(sydorenkoMother,sydorenkoFather,sydorenkoChildren,sydorenkoPets);

        Man chumakFather = new Man();
        Woman chumakMother = new Woman("Mother","Chumak",1996);
        Human chumakChild1 = new Human("Child1","Chumak",2017);
        Human chumakChild2 = new Human();
        Human chumakChild3 = new Human("Child3", "Chumak", 2022);
        Family chumakFamily = new Family();


        List<Family> families = new ArrayList<>(List.of(
                petrenkoFamilyWithoutChildren,
                sydorenkoFamily,
                chumakFamily
        ));
        CollectionFamilyDao familyDao = new CollectionFamilyDao(families);
        module = new FamilyService(familyDao);
    }

    @Test
    public void getAllFamilies(){
        String actual = module.getAllFamilies().toString();
        String expected = "[Family{mother=Human{name='Mother', surname='Petrenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Petrenko', DOB=1/1/1970, iq=0, schedule=null}, children=null, pet=no record}, Family{mother=Human{name='Mother', surname='Sydorenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Sydorenko', DOB=1/1/1970, iq=0, schedule=null}, children=[Human{name='childBoy', surname='Sydorenko', DOB=1/1/1970, iq=0, schedule=null}], pet=[DOMESTIC_CAT{nickname='null', age=0, trickLevel=0, habits=null}]}, Family{mother=Human{name='null', surname='null', DOB=1/1/1970, iq=0, schedule=null}, father=Human{name='null', surname='null', DOB=1/1/1970, iq=0, schedule=null}, children=null, pet=no record}]";
        assertEquals(expected, actual);
    }

    @Test
    void displayAllFamilies(){
        final PrintStream standardOut = System.out;
        final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStreamCaptor));
        String expected = "[0] Family{mother=Human{name='Mother', surname='Petrenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Petrenko', DOB=1/1/1970, iq=0, schedule=null}, children=null, pet=no record}\n" +
                "[1] Family{mother=Human{name='Mother', surname='Sydorenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Sydorenko', DOB=1/1/1970, iq=0, schedule=null}, children=[Human{name='childBoy', surname='Sydorenko', DOB=1/1/1970, iq=0, schedule=null}], pet=[DOMESTIC_CAT{nickname='null', age=0, trickLevel=0, habits=null}]}\n" +
                "[2] Family{mother=Human{name='null', surname='null', DOB=1/1/1970, iq=0, schedule=null}, father=Human{name='null', surname='null', DOB=1/1/1970, iq=0, schedule=null}, children=null, pet=no record}\n";
        module.displayAllFamilies();
        assertEquals(expected, outputStreamCaptor.toString());
    }

    @Test
    void getFamiliesBiggerThan(){
        String actual = module.getFamiliesBiggerThan(3).toString();
        String  expected = "[Family{mother=Human{name='Mother', surname='Sydorenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Sydorenko', DOB=1/1/1970, iq=0, schedule=null}, children=[Human{name='childBoy', surname='Sydorenko', DOB=1/1/1970, iq=0, schedule=null}], pet=[DOMESTIC_CAT{nickname='null', age=0, trickLevel=0, habits=null}]}]";
        assertEquals(expected,actual);
    }
    @Test
    void getFamiliesLessThan(){
        String actual = module.getFamiliesLessThan(3).toString();
        String  expected = "[Family{mother=Human{name='Mother', surname='Petrenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Petrenko', DOB=1/1/1970, iq=0, schedule=null}, children=null, pet=no record}, Family{mother=Human{name='null', surname='null', DOB=1/1/1970, iq=0, schedule=null}, father=Human{name='null', surname='null', DOB=1/1/1970, iq=0, schedule=null}, children=null, pet=no record}]";
        assertEquals(expected,actual);
    }
    @Test
    void countFamiliesWithMemberNumber(){
        int actual = module.countFamiliesWithMemberNumber(2);
        int  expected = 2;
        assertEquals(expected,actual);
    }

    @Test
    void createNewFamily(){
        module.createNewFamily(new Woman(),new Man());
        String actual = module.getFamilyById(module.getAllFamilies().size()-1).toString();
        String expected = "Family{mother=Human{name='null', surname='null', DOB=1/1/1970, iq=0, schedule=null}, father=Human{name='null', surname='null', DOB=1/1/1970, iq=0, schedule=null}, children=null, pet=no record}";
        assertEquals(expected,actual);
    }
    @Test
    void deleteFamilyByIndex(){
        String actual = module.getFamilyById(1).toString();
        module.deleteFamilyByIndex(0);
        String expected = "Family{mother=Human{name='Mother', surname='Sydorenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Sydorenko', DOB=1/1/1970, iq=0, schedule=null}, children=[Human{name='childBoy', surname='Sydorenko', DOB=1/1/1970, iq=0, schedule=null}], pet=[DOMESTIC_CAT{nickname='null', age=0, trickLevel=0, habits=null}]}";
        assertEquals(expected,actual);
    }

    /*
    @Test
    void bornChild(){
        Random randomNumberMock = Mockito.mock(Random.class);
        when(randomNumberMock.nextInt()).thenReturn(1);
        String actual = module.bornChild(module.getFamilyById(0),"Ania","Dima").toString();
        String  expected = "Family{mother=Human{name='Mother', surname='Petrenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Petrenko', DOB=1/1/1970, iq=0, schedule=null}, children=[Human{name='Dima', surname='Petrenko', DOB=1/1/1970, iq=0, schedule=null}], pet=no record}";
        assertEquals(expected,actual);
    }
     */

    @Test
    void adoptChild() {
        AdoptiveChild newChild = new AdoptiveChild("Girl","Ivanenko","20/03/2015", 78);
        String actual = module.adoptChild(module.getFamilyById(0),newChild).toString();
        String  expected = "Family{mother=Human{name='Mother', surname='Petrenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Petrenko', DOB=1/1/1970, iq=0, schedule=null}, children=[Human{name='Girl', surname='Ivanenko', DOB=20/3/2015, iq=78, schedule=null}], pet=no record}";
        assertEquals(expected,actual);
    }
    @Test
    void deleteAllChildrenOlderThan(){
        module.deleteAllChildrenOlderThan(10);
        String actual = module.getAllFamilies().toString();
        String expected = "[Family{mother=Human{name='Mother', surname='Petrenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Petrenko', DOB=1/1/1970, iq=0, schedule=null}, children=null, pet=no record}, Family{mother=Human{name='Mother', surname='Sydorenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Sydorenko', DOB=1/1/1970, iq=0, schedule=null}, children=[Human{name='childBoy', surname='Sydorenko', DOB=1/1/1970, iq=0, schedule=null}], pet=[DOMESTIC_CAT{nickname='null', age=0, trickLevel=0, habits=null}]}, Family{mother=Human{name='null', surname='null', DOB=1/1/1970, iq=0, schedule=null}, father=Human{name='null', surname='null', DOB=1/1/1970, iq=0, schedule=null}, children=null, pet=no record}]";
        assertEquals(expected,actual);
    }

    @Test
    void getFamilyById(){
        String actual = module.getFamilyById(0).toString();
        String expected = "Family{mother=Human{name='Mother', surname='Petrenko', DOB=1/1/1970, iq=78, schedule={WEDNESDAY=Meditate, MONDAY=Get Enough Sleep, THURSDAY=Workout, SUNDAY=Take Breaks To Re-energize, TUESDAY=Rise Early, FRIDAY=Eat A Good Breakfast, SATURDAY=Take A Nap}}, father=Human{name='Father', surname='Petrenko', DOB=1/1/1970, iq=0, schedule=null}, children=null, pet=no record}";
        assertEquals(expected,actual);
    }

    @Test
    void getPets(){
        String actual = module.getPets(1).toString();
        String expected = "[DOMESTIC_CAT{nickname='null', age=0, trickLevel=0, habits=null}]";
        assertEquals(expected,actual);
    }

    @Test
    void addPet(){
        module.addPet(0, new Dog());
        String actual = module.getPets(0).toString();
        String expected = "[DOG{nickname='null', age=0, trickLevel=0, habits=null}]";
        assertEquals(expected,actual);
    }

}
