package HW10;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class AdoptiveChild extends Human{

    public AdoptiveChild(String name, String surname, String birthDateInString, int iq) {
        this.name = name;
        this.surname = surname;
        setBirthDate(convertStringBirthDateToMilliseconds(birthDateInString));
        setIq(iq);

    }

    private long convertStringBirthDateToMilliseconds(String birthDateInString){
        boolean isEnteredDateValid = checkIfDateIsValid(birthDateInString);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try{
                String birthDateInStringWithTime = birthDateInString+" 00:00:01";
                Date date = sdf.parse(birthDateInStringWithTime);
                return date.getTime();
        } catch (ParseException e) {
            System.out.println("Date must have format dd/MM/yyyy. 01/01/1970 has been set by default.");
        }
        return 0L;
    }

    private static boolean checkIfDateIsValid(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setLenient(false);
        try {
            format.parse(date);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
}
